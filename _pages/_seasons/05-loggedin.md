---
layout: database
title: Login Accepted
permalink: loggedin
categories: db
permalink: /seasons/loggedin
type: Edit All Foods [Accessable after login, this page will be hidden from menu as well]
---

<html>
  <div class="row">
    <table class="table table-striped">
    	<thead>
    		<tr>
    			<th style="width:16%;"><span class="sort" data-sort="title">Food</span></th>
    			<th style="width:8%;"><span class="sort" data-sort="amount">Amount</span></th>
    			<th style="width:20%;"><span class="sort" data-sort="frequency">Frequency</span></th>
    			<th style="width:17%;"><span class="sort" data-sort="duration">Duration</span></th>
    			<th style="width:17%;"><span class="sort" data-sort="curr_price">Price</span></th>
    			<th style="width:15%;"><span class="sort" data-sort="price_cap">Price Cap</span></th>
    			<th style="width:15%;"><span class="sort" data-sort="edit_btn">Edit</span></th>
    		</tr>
    	</thead>
          	
      <tbody class="list">
          {% for page in site.pages %}
        	    {% if page.categories contains 'database' %}
              		<tr>
              			<td class="title">{{ page.title }}</td>
              			<td class="quality">{{ page.quantity }}</td>
              			<td class="frequency">{{ page.frequency }}</td>
              			<td class="duration">{{ page.duration }}</td>
              			<td class="curr_price">{{ page.curr_price }}</td>
              			<td class="price_cap">{{ page.price_cap }}</td>
                        <!-- THE EDIT BUTTONS WOULD ONLY SHOW FOR ADMIN USERS -->
                        <td><button class="editbtn">edit</button></td>
              		</tr>
      	    {% endif %}
          {% endfor %}
      </tbody>
    </table>
    
    <script>
      $(document).ready(function () {
        $('.editbtn').click(function () {
            var currentTD = $(this).parents('tr').find('td');
            if ($(this).html() == 'Edit') {                  
                $.each(currentTD, function () {
                    $(this).prop('contenteditable', true)
                });
            } else {
               $.each(currentTD, function () {
                    $(this).prop('contenteditable', false)
                });
            }
      
            $(this).html($(this).html() == 'Edit' ? 'Save' : 'Edit')
        });
      });
    </script>
    <a href="http://stackoverflow.com/questions/23572428/making-row-editable-when-hit-row-edit-button">Link</a>
  
  
  </div>
</html>