---
layout: database
title: Winter
date: 2017-02-11 17:40:00
categories: db
type: Winter Foods
permalink: /seasons/winter
---

<div class="row">
    <table class="table table-striped">
    	<thead>
    		<tr>
    			<th style="width:16%;"><span class="sort" data-sort="title">Food</span></th>
    			<th style="width:8%;"><span class="sort" data-sort="amount">Amount</span></th>
    			<th style="width:20%;"><span class="sort" data-sort="frequency">Frequency</span></th>
    			<th style="width:17%;"><span class="sort" data-sort="duration">Duration</span></th>
    			<th style="width:17%;"><span class="sort" data-sort="curr_price">Price</span></th>
    			<th style="width:15%;"><span class="sort" data-sort="price_cap">Price Cap</span></th>
    		</tr>
    	</thead>
            
        <tbody class="list">
            {% for page in site.pages %}
          	    {% if page.tags contains 'winter' %}
                		<tr>
                			<td class="title">{{ page.title }}</td>
                			<td class="quality">{{ page.quantity }}</td>
                			<td class="frequency">{{ page.frequency }}</td>
                			<td class="duration">{{ page.duration }}</td>
                			<td class="curr_price">{{ page.curr_price }}</td>
                			<td class="price_cap">{{ page.price_cap }}</td>
                		</tr>
        	    {% endif %}
            {% endfor %}
        </tbody>
    </table>
</div>