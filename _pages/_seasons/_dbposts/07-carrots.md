---
layout: default
title: Carrots
date: 2017-02-11 17:40:00
categories: database
tags: [summer, fall, winter, spring]

quantity: 20
frequency: Saturdays [5am-10am]
duration: Until March, 2nd
curr_price: $5/bag
price_cap: 1 bag
---