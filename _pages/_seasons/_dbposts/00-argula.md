---
layout: default
title: Argula
date: 2017-02-11 17:40:00
categories: database
tags: [fall, winter, spring]

quantity: 10
frequency: Saturdays [5am-10am]
duration: 10 Weeks
curr_price: $10/argula
price_cap: 10 argulas
---