---
layout: default
title: Cantelope
date: 2017-02-11 17:40:00
categories: database
tags: [summer]

quantity: 75
frequency: Saturdays [5am-10am]
duration: 4 Weeks
curr_price: $1.50/cantelope
price_cap: 20 cantelopes
---