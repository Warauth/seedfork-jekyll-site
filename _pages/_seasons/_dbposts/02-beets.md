---
layout: default
title: Beets
date: 2017-02-11 17:40:00
categories: database
tags: [fall, winter, spring]

quantity: 5
frequency: Mondays [5am-7am]
duration: Until March, 15th
curr_price: $1/beet
price_cap: 20 beets
---