---
layout: default
title: Brussel Sprouts
date: 2017-02-11 17:40:00
categories: database
tags: [fall, winter, spring]

quantity: 23
frequency: Mondays [5am-7am]
duration: Until May, 25th
curr_price: $0.50/sprout
price_cap: 5 sprouts
---