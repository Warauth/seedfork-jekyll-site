---
layout: default
title: Broccoli
date: 2017-02-11 17:40:00
categories: database
tags: [summer, fall, winter, spring]

quantity: 50
frequency: Mondays [5am-10am]
duration: Until March, 15th
curr_price: $1/stem
price_cap: 20 stems
---