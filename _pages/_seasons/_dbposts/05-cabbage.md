---
layout: default
title: Cabbage
date: 2017-02-11 17:40:00
categories: database
tags: [summer, fall, winter, spring]

quantity: 50
frequency: Saturdays [5am-10am]
duration: Until April, 10th
curr_price: $1.50/head
price_cap: 2 heads
---