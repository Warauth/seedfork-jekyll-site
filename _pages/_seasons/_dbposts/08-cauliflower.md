---
layout: default
title: Cauliflower
date: 2017-02-11 17:40:00
categories: database
tags: [fall, spring]

quantity: 20
frequency: Saturdays [5am-10am]
duration: Until April, 20th
curr_price: $1/head
price_cap: 12 heads
---