---
layout: default
title: Chicory
date: 2017-02-11 17:40:00
categories: database
tags: [fall, winter, spring]

quantity: 50
frequency: Saturdays [5am-8am]
duration: Until May, 20th
curr_price: $10/bag
price_cap: 5 bag
---