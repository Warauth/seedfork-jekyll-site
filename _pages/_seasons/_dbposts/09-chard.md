---
layout: default
title: Chard
date: 2017-02-11 17:40:00
categories: database
tags: [winter, spring]

quantity: 200
frequency: Saturdays [5am-12pm]
duration: 6 Weeks
curr_price: $5/chard
price_cap: 20 chards
---