---
layout: database
title: Login
permalink: login
categories: db
permalink: /seasons/login
type: Login [this page will be hidden from menu, create user will be hidden initially as well]
---

<html>
    <head>
        <title>Login</title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    
    <body>
        <div class="database" align="center">
            <form action="create.php" method="post">
                <input type="text"  placeholder="username" name="user"><br>
                <input type="password" placeholder="password" name="pass"><br><br>
                <input type="submit" value="Login">
                <input type="button" value="Create" onclick="myFunction()"><br><br><br>
            </form>
            
            <div id="hidden">
               <!--<form action="main/create.php" method="post">-->
               <form method="create">
                   <input type="text"  placeholder="username" name="user"><br>
                   <input type="password" placeholder="password" name="pass"><br><br>
                   <input type="submit" value="Create Account"><br><br><br>
               </form>
            </div>
            
            <script>
                function myFunction() {
                    var x = document.getElementById('hidden');
                    if (x.style.display === 'none') {
                        x.style.display = 'block';
                    } else {
                        x.style.display = 'none';
                    }
                }
            </script>
        </div>
    </body>
</html>
