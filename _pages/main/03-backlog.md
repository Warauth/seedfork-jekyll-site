---
layout: default
title: Backlog
permalink: /backlog/
catagories: main
---

Here is the backlog for the Seedfork project:

- Archive - only let admins update/delete information  
- Search - remove the white out search, it's garbage  
- Admin Creation - change how to create an admin/truck driver
- Change Column Sort - when it is a number, do an integer sort
- Button to insert into truck extremely quickly (Acquire Button)
- Insert into TRUCK instead of going straight to ARCHIVES - Completed
- ALERTS for pickups and matches
- Update rows after account change by user (all tables)
- in archives, split deliveries if half gets dropped off at pantry and half at another
- Hover options for produce insertions, as well as names underneath - Completed
- Add QUICK EDIT on Truck Inventory page (modal to pop up with options)
- Put ALL CSS and JavaScript Styling on ONE PAGE
- Add CARACAL to download Archives Page