---
layout: page
title: Home
permalink: /home/
categories: main
---

<h3>SEEDFORK SEMESTER PROJECT</h3>
<h5>Group Name: SeedFork Blue</h5>
    Group Members:  
    Justin Foster  
    Spencer Cothran  
    Gage Garner  
    
<br>
<a href="https://trello.com/b/bKSC3mS7/project">Trello Scrumboard</a><br>
<a href="https://drive.google.com/open?id=0BxAFcRgIhHEGX1BWRXhfRzBXWjA">Google Drive Notes</a>
<br>
[Link to the youtube demonstration of the prototype.](https://www.youtube.com/watch?v=PfxrEgRzVLA)
<br>
[Link to the youtube demonstration of the intermediate release.](https://www.youtube.com/watch?v=aGvQzMhrbHE&t=460s)
<br>
<br>

    Customer Contact Information:
    Name: Ellen Wolfe
    Email: ellenpwolfe@frontiernet.net
    Phone: 931.261.2111
    Meetings: Every Thursday