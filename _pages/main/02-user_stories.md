---
layout: default
title: User Stories
permalink: /userstories/
categories: main
---

# These are our User Stories, prioritized by MoSCoW:

<br>

## M: Must

- #### As a Farmer, I would like to (must) be able to post what/how much excess produce I have available, so that Seedfork's Truck Driver can pick it up.  
- #### As a Farmer, I need to be able to edit what produce/amounts of produce I have available, in case I accidentally add a wrong value or produce type.
- #### As a Truck Driver, I would like to know all the addresses of the Pantries, so I can choose which Pantry to deliver to.
- #### As a Truck Driver, I would like to know the addresses of the Farmers, so I can know where to pick up their food.
- #### As an Admin, I would like to be able to approve Users, so I can prevent individuals from requesting food.
- #### As an Admin, I would like to be able to edit entries in the database, so that I can change errors in Farmers' and Pantries' entries.

<br>

## S: Should

- #### As a User who could visit the site, I would like to be able to see what has been delivered, so I can get the produce from that Pantry.

<br>

## C: Could

- #### As a Planter, I would  like to (must) be able to post what/how much excess produce I have available, so that Seedfork's Truck Driver can pick it up.  
- #### As a Planter, I need to be able to edit what produce/amounts of produce I have available, in case I accidentally add a wrong value or produce type.  
- #### As a Grower, I would like to (must) be able to post what/how much excess produce I have available, so that Seedfork's Truck Driver can pick it up.  
- #### As a Grower, I need to be able to edit what produce/amounts of produce I have available, in case I accidentally add a wrong value or produce type.
- #### As an Organization, I would like to be able to request certain kinds of produce, so that I can receive the produce that I need.  
- #### As an Organization, I need to be able to edit what produce/amounts of produce I need, in case I accidentally add a wrong value or produce type.
- #### As a Random User who won't create an account, I would like to be able to see what produce is available, so that I can see which pantries are getting what produce, and go to them.

<br>

## W: Won't

- #### As the Seedfork Connection Developer Team, we will NOT create an app for Seedfork of the Highlands, because it would be overkill.
- #### As the Seedfork Connection Developer Team, we will NOT add support for Restaurants to create requests, as they are a secondary concern, and getting the produce to those who are in desperate need of them is our top priority.
- #### As the Seedfork Connection Developer Team, we will NOT match Pantries with produce, as that is not what the customer wants.
- #### As the Seedfork Connection Developer Team, we will NOT let Pantries make accounts, as that is no longer what the customer wants.

<!-- OLD USER STORIES FOR PANTRIES (in case we need to add them back later)
#### As a Pantry, I would like to be able to request certain kinds of produce, so that I can receive the produce that I need.
- #### As a Pantry, I need to be able to edit what produce/amounts of produce I need, in case I accidentally add a wrong value or produce type. -->