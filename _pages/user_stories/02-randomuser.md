---
layout: userstory
title: RandomUser
permalink: /user_stories/randomuser
categories: userstory
goal: stumble onto the page that shows the database of foods,
reason: if I want to make dinner with fresh tomatoes, I can go to the website database and see when/where I can get them.
---