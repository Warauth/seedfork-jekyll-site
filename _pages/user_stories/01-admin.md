---
layout: userstory
title: Admin
permalink: /user_stories/admin
categories: userstory
goal: be able to update the website/database,
reason: it is always up-to-date.
---