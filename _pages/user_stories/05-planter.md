---
layout: userstory
title: Planter
permalink: /user_stories/planter
categories: userstory
goal: be able to donate or sell my excess foods,
reason: they do not go to waste.
---