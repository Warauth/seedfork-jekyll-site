---
layout: userstory
title: Farmer
permalink: /user_stories/farmer
categories: userstory
goal: I want to be able to tell people what food I have available, and how much,
reason: I distribute it accordingly to people who can benefit from it.
---