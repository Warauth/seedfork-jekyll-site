---
layout: userstory
title: SeedForkUser
permalink: /user_stories/seedforkuser
categories: userstory
goal: be able to input information into the database about who has what foods,
reason: so that our users can see what foods we have available or where they are available at.
---