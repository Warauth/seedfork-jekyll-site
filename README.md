## This is the SeedFork Jekyll site for (self-named) SeedFork Team Blue, also known as "The SeedFork Connection".  
This website is for our project documentation.  
  
To run the website after cloning the repository, enter the following command at the terminal (bash):
```
$ ./serve.sh
```
to run the script, OR:
```
$ jekyll serve --host $IP --port $PORT
```
OR:
```
$ bundle exec jekyll serve
```
to run the command manually.
