---
layout: post
title: "Status Update: March 1, 2017"
date: 2017-04-13 8:35:00 +0000
salutation: "Sincerely"
author: SeedFork Team Blue
categories: spring17 assignments
tags: assignments, inclass
---

Just a little update on the project:  
- Our intermediate release is almost complete! Just some minor adjustments need to be made, and we can make the shift to the full release.
- The YouTube video tutorial is online, and gives a decent idea of how to full release will operate. The link is on the homepage.
- We are still working on the styling. It's mostly done, but some work still remains.

Overall, the project has been moving along well. The project should easily be done in time for the final iteration and full release.