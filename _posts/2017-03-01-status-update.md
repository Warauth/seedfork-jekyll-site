---
layout: post
title: "Status Update: March 1, 2017"
date: 2017-03-01 8:35:00 +0000
salutation: "Sincerely"
author: SeedFork Team Blue
categories: spring17 assignments
tags: assignments, inclass
---

Just a little update on the project:  
  
- We've made good work on the database on the project website. Honestly, there is not much left to do (hopefully), and won't take long to finish.  
- The InVision prototye is in a good state - we ought to be able to give a good presentation in class for the client today.  
- The team has been working together well. As long as that continues, we'll be able to knock this project out of the park!  
  
We just wanted to share how everything has been going with the project.
