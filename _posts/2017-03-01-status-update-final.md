---
layout: post
title: "Status Update: March 1, 2017"
date: 2017-05-01 17:10:00 +0000
salutation: "Sincerely"
author: SeedFork Team Blue
categories: spring17 assignments
tags: assignments, inclass
---

This is the final update for our iteration of the project.

- The project is nearly complete. All that is left are the backlogged features, which can be found in the Backlog tab on the navbar.
- The base for the app is complete. It can do most everything that Ellen (the client) asked for. The remaining features are just quality of life changes.

We hope that this project will be carried over to the Software Engineering class, and that they would be able to add the features that we didn't get to implement.
