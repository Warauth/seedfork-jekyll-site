<html>
<head>
  <title>TEST DATABASE</title>
</head>
<body>
<h1>Testing Database</h1>
<?php


  @ $db = new mysqli('sql9.freesqldatabase.com', 'sql9158834', 'MDFn9Yxb3X', 'sql9158834');

  if (mysqli_connect_errno()) {
     echo 'Error: Could not connect to database.  Please try again later.';
     exit;
  }

  $query = "select * from food_test";
  $result = $db->query($query);

  $num_results = $result->num_rows;

  echo "<p>Number of books found: ".$num_results."</p>";

  for ($i=0; $i <$num_results; $i++) {
     $row = $result->fetch_assoc();
     echo "<p><strong>".($i+1).". Food: ";
     echo htmlspecialchars(stripslashes($row['food']));
     echo "</strong><br />Quantity: ";
     echo stripslashes($row['quantity']);
     echo "<br />Frequency: ";
     echo stripslashes($row['frequency']);
     echo "<br />Price: ";
     echo stripslashes($row['price']);
     echo "</p>";
  }

  $result->free();
  $db->close();

?>
</body>
</html>